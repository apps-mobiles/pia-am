import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EpisodesPage } from './episodes.page';

const routes: Routes = [
  {
    path: '',
    component: EpisodesPage
  },
  {
    path: 'episode-details',
    loadChildren: () => import('./episode-details/episode-details.module').then( m => m.EpisodeDetailsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EpisodesPageRoutingModule {}
