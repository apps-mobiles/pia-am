import { Planet } from './planets.model';
import { PlanetsService } from './planets.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.page.html',
  styleUrls: ['./planets.page.scss'],
})
export class PlanetsPage implements OnInit {

  planets = [];
  
  constructor(private planetsService: PlanetsService) { }

  ngOnInit() {
    this.planetsService.getURL().subscribe(
      data => {
        this.planets = data;
      }
    )
  }
}
