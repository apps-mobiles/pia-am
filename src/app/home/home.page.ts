import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private route: Router) {}

  goCharacters() {
    this.route.navigate(['/characters']);
  }

  goPlanets() {
    this.route.navigate(['/planets']);
  }

  goEpisodes() {
    this.route.navigate(['/episodes']);
  }
  
}
